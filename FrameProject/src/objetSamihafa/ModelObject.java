/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetSamihafa;

import java.lang.reflect.Method;

/**
 *
 * @author drana
 */
public class ModelObject {
    private Class laClass;
    private Method laMethod;

    public Class getLaClass() {
        return laClass;
    }

    public void setLaClass(Class laClass) {
        this.laClass = laClass;
    }

    public Method getLaMethod() {
        return laMethod;
    }

    public void setLaMethod(Method laMethod) {
        this.laMethod = laMethod;
    }
    
}
