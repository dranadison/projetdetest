/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetSamihafa;

import java.util.HashMap;

/**
 *
 * @author drana
 */
public class ModelView {
    private String url;
    private HashMap<String,Object> hasmap;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String,Object> getHasmap() {
        return hasmap;
    }

    public void setHasmap(HashMap<String,Object> hasmap) {
        this.hasmap = hasmap;
    }
    
}
