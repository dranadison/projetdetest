/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author drana
 */
public class UrlNotSupportedException extends Exception{
    private String message="Follow this structure of URL \"methodName-className.do\" or check the values of annotations.";
    public String getMessage() {
        return message;
    }
}
