package util;


import exception.UrlNotSupportedException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Vector;
import annotation.UrlFonction;
import annotation.UrlClasse;
import java.util.Set;
import objetSamihafa.ModelObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author drana
 */
public class ClassUtilitaire {
    //Feature 1;
    public String retrieveUrlFromRawUrl(String url){
        
        String[] tabRep=url.split("/");
        String [] tab2=tabRep[tabRep.length-1].split(".do");
        return tab2[0];
    }
    //Recursiffffffffffffff
    public static Class[] getClassList(String path,String pack) throws MalformedURLException, ClassNotFoundException{
        URL url=new URL("file:///"+path+"\\WEB-INF\\classes\\"+pack);
        File dir=new File(url.getFile());
        String[] contents=dir.list();
        Class[] val=new Class[contents.length];
        for(int i=0;i<val.length;i++){
            Class c=Class.forName(pack+"."+contents[i].replace(".class", ""));
            val[i]=c;
        }
        return val;
    }
    public static HashMap<String,Class> getAnnotedClass(Class[] classes) throws UrlNotSupportedException
    {
        HashMap<String,Class> h=new HashMap();
        for(int i=0;i<classes.length;i++){
            Class c=classes[i];
            if(c.isAnnotationPresent(UrlClasse.class))
            {
                UrlClasse a=(UrlClasse) c.getAnnotation(UrlClasse.class);
                
                h.put(a.value(), c);
               
            }
        }
         return h;
    }
    
    public static HashMap<String,Method> getAnnotedMethod(Class c) throws UrlNotSupportedException{
        if(c==null){
            UrlNotSupportedException e=new UrlNotSupportedException();
            throw e;
        }
        HashMap<String,Method> h=new HashMap();
        Method[] methods=c.getDeclaredMethods();
        for(int i=0;i<methods.length;i++)
        {
            Method m=methods[i];
            if(m.isAnnotationPresent(UrlFonction.class))
            {
                UrlFonction a=(UrlFonction) m.getAnnotation(UrlFonction.class);
                
                
                h.put(a.value(), m);
                
            }
        }
        return h;
    }
//    S2-feature1:
    public HashMap<String,ModelObject> mamerinaHmap(String path,String nomPackage) throws Exception
    {
        Class[] classes=this.getClassList(path,nomPackage);
        HashMap<String,Class> leClassAnnoted=this.getAnnotedClass(classes);
        Set<String> zavatra=leClassAnnoted.keySet();
       
        
        HashMap<String,ModelObject> h=new HashMap();
        
        
        for (String string : zavatra) 
        {
            HashMap<String,Method> meth=this.getAnnotedMethod(leClassAnnoted.get(string));
            
            Set<String> zavatra1=meth.keySet();
            for (String string1 : zavatra1) 
            {
                 String url=string+"-"+string1;
        
                ModelObject mo=new ModelObject();
                mo.setLaClass(leClassAnnoted.get(string));
                mo.setLaMethod(meth.get(string1));
                h.put(url, mo);
            }
        }
        return h;
    }
    public String toUpper(String s)
	{
		return s.substring(0,1).toUpperCase()+s.substring(1);
	}
    //SPRINT2
    //F1---~
    //fonction mamerina anle hmap (url,object)
    //claasMahazaka Class-Method
    //le fonction mamerina anle hmap no ilaina atao anaty init.
    
    //F2---gettena le url, d jerena amle liste anle hmap, testena ny existenceny  na tsia, 
    //rah misy--> 
    
    //F3------modelView url(anaovana anle dispatch)- hmap 
    //alaina le class, invokena le methode, 
    
    
    //sprint3
    //
  
}
