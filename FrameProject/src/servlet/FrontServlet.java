package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import objetSamihafa.ModelObject;
import objetSamihafa.ModelView;
import util.ClassUtilitaire;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author drana
 */
public class FrontServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init(); 
        
         String nomPackage="objetSamihafa";
         ServletContext contexte=this.getServletContext();
         ClassUtilitaire classUtilitaire=new ClassUtilitaire();
         
          String path=this.getServletContext().getRealPath("");

          if(contexte.getAttribute("Hashmap")==null)
            {
                try {
                HashMap<String,ModelObject> h=classUtilitaire.mamerinaHmap(path, nomPackage);                
                contexte.setAttribute("Hashmap",h);
                
                } catch (Exception ex) {
                    Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else
            {
                HashMap<String,ModelObject> h=(HashMap)contexte.getAttribute("Hashmap");
            }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
             ServletContext contexte=this.getServletContext();
           
             //test S2-F1
             HashMap<String,ModelObject> h=(HashMap)contexte.getAttribute("Hashmap");
             
            ClassUtilitaire classUtilitaire=new ClassUtilitaire();
            String url=classUtilitaire.retrieveUrlFromRawUrl(request.getRequestURI());
            
            ModelObject mo=h.get(url); 
            Class c=null;
            Method m=null;
            ModelView mmo=null;
            String urlDirection=new String();
            HashMap<String,Object> settena=null;
            
            //test S2-F2
            if(mo!=null)
            {
               c=mo.getLaClass();
               m=mo.getLaMethod();
               
                mmo=(ModelView)(m.invoke(c.newInstance()));
                urlDirection=mmo.getUrl();
                settena=mmo.getHasmap();
                for (HashMap.Entry<String, Object> value : settena.entrySet()) 
                {
                    String key=value.getKey();
                    Object val=(Object)value.getValue();
                    if(val!=null)
                    {
                        request.setAttribute(key, val);
//                        out.println("key="+key);
//                        out.println("--val="+val);
//                        out.println("---------------");
                        
                    }
                }
            }
            
           
              //Sprint3
            Field[] attr=c.getDeclaredFields();
            Object o=c.newInstance();
            
            for(int i=0;i<attr.length;i++)
            {
                String name=attr[i].getName();
                out.println("-attr="+attr.length);
                
                String val=request.getParameter(name);
                if(val!=null)
                {
                 out.println("-request="+val);
                    String type=attr[i].getType().getSimpleName().toString();
                 out.println("-type="+type+"");
                 out.println("-type2222="+"String");
                    Method set=c.getMethod("set"+classUtilitaire.toUpper(name),attr[i].getType());
                    Object obj=new Object();
                    if(type.equals("int")){
                        obj=Integer.parseInt(val);
                    }
                    else if(type.equals("double")){
                        obj=Double.parseDouble(val);
                    }
                    else if(type.equals("float")){
                        obj=Float.parseFloat(val);
                    }
                    else if(type.equals("String")){
                        obj=val;
                    out.println("-meth="+set.getName());
                    out.println("-obj="+obj);
                    }
                    set.invoke(o, obj);
                }
            }
            mmo=(ModelView) m.invoke(o);
                urlDirection=mmo.getUrl();
                settena=mmo.getHasmap();
                for (HashMap.Entry<String, Object> value : settena.entrySet()) 
                {
                 String key=value.getKey();
                 Object valinyy=(Object)value.getValue();
                 request.setAttribute(key, valinyy);
                }
                
                RequestDispatcher dispat = request.getRequestDispatcher(urlDirection);
                dispat.forward(request,response);
       
    }
         catch (Exception ex) {
            Logger.getLogger(servlet.FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
